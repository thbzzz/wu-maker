#!/usr/bin/python3
#
# That script is meant to make easier the task of creating a writeup about a
# Hack The Box machine. It can connect with a Hugo static website and fill in
# advance the index.md file with the skeleton of the writeup. The box
# information is parsed and its logo is downloaded, so you just have to write.
#
# Hugo framework: https://gohugo.io/
# Hack The Box: https://www.hackthebox.eu/

import os
import shutil
import subprocess
import time
import requests
import yaml
import webbrowser
from bs4 import BeautifulSoup
from jinja2 import Environment, FileSystemLoader
from getpass import getpass
from docopt import docopt

DOC = """HTB WU Maker

Usage:
  wu_maker.py -b <boxid> -w <hugo> -n <nmap> -t <tags>...

Options:
  -b  The box id on HTB (e.g. 259)
  -w  The path of the Hugo website root directory on your machine (e.g. ~/thbz.fr)
  -n  The path of the nmap scan output file on your machine (e.g. ~/htb/tabby/scan.nmap)
  -t  Tags that define the writeup (e.g. kerberoast, sqli, ...)
  -h  Help
"""


class HTBConnector:
    """Class for connection and scrapping of HTB."""

    def __init__(self):
        """Return nothing.
        
        Defines login URL and needed HTTP headers, declares box_info dict.
        """
        # Login page
        self.login_url = "https://www.hackthebox.eu/login"
        # Headers to avoid being blocked as a bot
        self.__headers = {
            "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0",
            "Host": "www.hackthebox.eu",
            "Referer": "https://www.hackthebox.eu/login",
        }
        # Data to be scrapped on the box page
        self.box_info = {
            "name": None,
            "logo": None,
            "os": None,
            "difficulty": None,
            "points": None,
            "release": None,
            "ip": None,
            "creators": [],
        }

    # Create HTB session
    def __connect(self):
        """Return nothing.
        
        Prompts the user for email and password, then tries to connect to HTB.
        Saves a requests.sessions.Session attribute.
        """
        # Get user email and password
        email = "thbz@protonmail.com"
        password = getpass("password: ")
        # Comment the above and uncomment the following line to set
        # hard-coded password (do not push it in any git repo)
        # password = "bouzygouloumdidnothingwrong"

        # Initialize session, the CSRF token needs to be retrieved
        self.__session = requests.Session()
        page = self.__session.get(self.login_url, headers=self.__headers)
        soup = BeautifulSoup(page.text, "html.parser")

        # Get the CSRF Token
        token = soup.find("input", {"name": "_token"}).get("value").strip()

        # Create form data
        login_form = {"_token": token, "email": email, "password": password}

        # Send data and login
        login_page = self.__session.post(
            self.login_url, headers=self.__headers, data=login_form
        )

        if "These credentials do not match our records." in login_page:
            print("error: authentication failed. Check your login/password.")
            exit(1)

    def __get_box_info(self, url):
        """Return nothing.

        Scraps the specified box page URL, and extracts the following parameters
        into a dict attribute:
        - name
        - logo url
        - os
        - difficulty
        - points
        - release date
        - ip
        - creators
        """

        box_page = self.__session.get(
            url, headers=self.__headers, allow_redirects=False
        )

        if box_page.status_code == 200:
            soup = BeautifulSoup(box_page.text, "html.parser")

            # The machine card (button "Info Card") contains main information
            machine_card = soup.find("div", {"id": "machineCard"})
            machine_card_tr = machine_card.findAll("tr")

            # Box name
            self.box_info["name"] = machine_card.find("h2").text.strip()
            # Logo URL
            self.box_info["logo"] = machine_card.find("img").get("src")
            # OS
            self.box_info["os"] = (
                machine_card_tr[0].findAll("td")[1].text.strip()
            )
            # Difficulty
            self.box_info["difficulty"] = (
                machine_card_tr[1].findAll("td")[1].text.strip()
            )
            # Points
            self.box_info["points"] = (
                machine_card_tr[2].findAll("td")[1].text.strip()
            )
            # Release date
            self.box_info["release"] = (
                machine_card_tr[3].findAll("td")[1].text.strip()
            )
            # IP
            self.box_info["ip"] = (
                machine_card_tr[4].findAll("td")[1].text.strip()
            )
            # Creators (not in machine card)
            creators = soup.findAll(
                "div", {"class": "col-md-12 col-xs-12 text-center"}
            )[1].findAll("a")
            self.box_info["creators_str"] = " & ".join(
                "[{}]({})".format(c.string, c["href"]) for c in creators
            )
            for c in creators:
                self.box_info["creators"].append(
                    {"name": c.string, "url": c["href"]}
                )

            # Table summarizing box information
            self.box_info[
                "box_info_table"
            ] = """Name|OS|Difficulty|Points|Release|IP|Creator(s)
            ---|---|---|---|---|---|---
            {}|{}|{}|{}|{}|{}|{}
            """.format(
                self.box_info["name"],
                self.box_info["os"],
                self.box_info["difficulty"],
                self.box_info["points"],
                self.box_info["release"],
                self.box_info["ip"],
                self.box_info["creators_str"],
            )
        else:
            print(
                "error: cannot reach box page. HTTP code: {}".format(
                    box_page.status_code
                )
            )
            exit(1)

    def download_logo(self, url, dst):
        """Return nothing.

        Downloads the box logo from the specified URL,
        and copies the file to specified destination path.
        """
        logo = self.__session.get(url, stream=True)
        if logo.status_code == 200:
            # Write the image as bytes
            logo.raw.decode_content = True
            with open(dst, "wb") as target:
                shutil.copyfileobj(logo.raw, target)
        else:
            print(
                "error: cannot download logo. HTTP code: {}".format(
                    logo.status_code
                )
            )
            if logo.status_code == 404:
                print(
                    "+ this may be fixed by visiting {} in your web browser.".format(
                        url
                    )
                )
                yn = input(
                    "do you want to open this url in you browser and retry? (y/N) "
                )
                if yn.lower() == "y":
                    webbrowser.open(url)
                    print("please launch the same command again.")
            exit(1)

    def run(self, url):
        """Return a dictionary containing box information.
        
        Creates a session with HTB with provided credentials,
        scraps the specified box page and store useful data
        in a dictionary attribute.
        """
        # Create session and scrap challenge page
        print("+ authenticating...")
        self.__connect()
        print("+ parsing {}...".format(url))
        self.__get_box_info(url)
        return self.box_info


class WUMaker:
    """Class used for the creation of the writeup into Hugo's index.md"""

    def __init__(self, wumaker_location, wu, wuvars, nmap, tags):
        """Return nothing.
        
        Initializes core variables and starts __render_front_matter().
        """
        # Directory path containing this script
        self.wumaker_location = wumaker_location
        self.wu = wu  # WU file path (index.md in Hugo)
        self.wuvars = wuvars  # Variables to fill the WU with
        self.nmap = nmap  # Nmap scan file path
        self.tags = tags  # WU tags

        # Retrieve the existing front matter from the article
        with open(self.wu) as target:
            # Remove --- and load yaml
            self.wuvars["front_matter"] = target.read().replace("---", "")
            self.wuvars["front_matter"] = yaml.safe_load(
                self.wuvars["front_matter"]
            )

        # Get content of nmap scan
        with open(self.nmap) as f:
            self.wuvars["nmap"] = "".join(
                [l for l in f.readlines() if not l.startswith("#")]
            ).strip()

        # Render front matter variables
        self.__render_front_matter(self.tags)

    def __render_front_matter(self, tags):
        """Return nothing.
        
        Initializes the front matter values according to box information.
        """
        # Title
        self.wuvars["front_matter"]["title"] = "HTB: {}".format(
            self.wuvars["box"]["name"].capitalize()
        )
        # Date
        self.wuvars["front_matter"]["date"] = (
            time.strftime("%Y-%m-%dT%H:%M:%S")
            + time.strftime("%z")[:3]
            + ":00"
        )
        # Logo
        self.wuvars["front_matter"]["logo"] = "img/logo.png"
        # Tags
        self.wuvars["front_matter"]["tags"] = ["htb"] + tags
        # Draft
        self.wuvars["front_matter"]["draft"] = True
        # Summary
        self.wuvars["front_matter"][
            "summary"
        ] = "{} {} box about {}. Created by {}.".format(
            self.wuvars["box"]["os"].capitalize(),
            self.wuvars["box"]["difficulty"].lower(),
            ", ".join(self.wuvars["front_matter"]["tags"][1:]),
            self.wuvars["box"]["creators_str"],
        )

    def run(self):
        """Return nothing.
        
        Loads jinja template, renders it and writes it to disk.
        """
        # Load template
        print("+ loading template...")
        template = Environment(
            loader=FileSystemLoader(self.wumaker_location, followlinks=True)
        ).get_template("index.md.jinja")

        # Render template
        print("+ rendering template...")
        render_template = template.render(values=self.wuvars)

        # Write template
        with open(self.wu, "w") as target:
            target.write(render_template)


if __name__ == "__main__":
    args = docopt(DOC)

    # Initialize HTBConnector object
    htbconnector = HTBConnector()

    # Scrap box page
    box_info = htbconnector.run(
        "https://www.hackthebox.eu/home/machines/profile/{}".format(
            args["<boxid>"]
        )
    )

    # Variables
    wu_slug = "htb_{}".format(htbconnector.box_info["name"].lower())
    wu_path = os.path.join(
        os.path.join(args["<hugo>"], "content/writeups"), wu_slug
    )
    wuvars = {
        "box": htbconnector.box_info,
    }

    # Create new WU with Hugo
    subprocess.run(
        [
            "hugo",
            "-s",
            args["<hugo>"],
            "new",
            os.path.join("writeups", wu_slug),
            "--kind",
            "htb",
        ]
    )

    # Download the logo and put it in the img folder of the new WU
    htbconnector.download_logo(
        box_info["logo"], os.path.join(wu_path, "img/logo.png"),
    )

    # Initialize WUMaker object
    wu_maker = WUMaker(
        os.path.join(
            os.path.dirname(os.path.abspath(os.path.realpath(__file__))), ""
        ),
        os.path.join(wu_path, "index.md"),
        wuvars,
        args["<nmap>"],
        args["<tags>"],
    )

    # Fill and write template
    wu_maker.run()
