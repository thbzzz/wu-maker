# Hack the Box WU maker

## Summary

- [Installation](./README.md#installation)
- [Usage](./README.md#usage)
- [Example](./README.md#example)

## Installation

First of all you have to clone this repository (don't forget to add your SSH key in gitlab). Then you have to install the Python packages, and set the script as executable.

```bash
git clone git@gitlab.com:thbzzz/wu-maker.git
cd wu-maker
pip3 install -r requirements.txt --user
chmod +x ./wu_maker.py
```

And you're good to go!

## Usage

The tool will use [Hugo](https://gohugo.io/) to create a new writeup, then it will be filled with the information scrapped from [Hack The Box](https://www.hackthebox.eu/).

To create a new writeup you have to provide the following options:
- **box id**: the id of the box on HTB (e.g.  `259`, the script will reach https://www.hackthebox.eu/home/machines/profile/259)
- **hugo**: The location of the Hugo website directory on your PC (e.g. `~/thbz.fr`)
- **nmap**: The location of the nmap scan on your PC (e.g. `~/htb/tabby/scan.nmap`)
- **tags**: Tags to be included in your article (e.g. `nosql ssti logrotate`)

The template file `index.md.jinja` is very easy to customize, as well as the source code which is quite short. You can adapt everything, but it will remain faster for Hugo users.

## Example

This is an example for the Tabby box.

```bash
./wu_maker.py -b 259 -w ~/thbz.fr -n ~/htb/tabby/scan.nmap -t tag1 tag2
```

In this example, the writeup will be created in `~/thbz.fr/content/writeups/htb_tabby/`.

The script will download the box logo and put it in `img/logo.png` under the writeup directory. Besides, the `index.md` file will be filled with this content:

```
---
title: "HTB: Tabby"
date: 2020-07-30T01:56:44+02:00
summary: "Linux easy box about tag1, tag2. Created by [egre55](https://www.hackthebox.eu/home/users/profile/1190)."
logo: "img/logo.png"
tags: ['htb', 'tag1', 'tag2']
draft: True
---

## Box info
Name|OS|Difficulty|Points|Release|IP|Creator(s)
---|---|---|---|---|---|---
Tabby|Linux|Easy|20|20 Jun 2020|10.10.10.194|[egre55](https://www.hackthebox.eu/home/users/profile/1190)
            

## Foothold
### Nmap
The usual `nmap` command, to know where to start:

```bash
# Nmap 7.60 scan initiated Fri Jul 10 12:56:46 2020 as: nmap -sS -sV -sC -p- -vvv --min-rate 5000 --reason -oN scan.nmap 10.10.10.194
Increasing send delay for 10.10.10.180 from 0 to 5 due to 142 out of 473 dropped probes since last increase.
Increasing send delay for 10.10.10.180 from 5 to 10 due to 181 out of 601 dropped probes since last increase.
Warning: 10.10.10.180 giving up on port because retransmission cap hit (10).
Nmap scan report for remote.htb (10.10.10.180)
Host is up, received echo-reply ttl 127 (0.18s latency).
Scanned at 2020-07-10 12:56:46 CEST for 217s
Not shown: 53899 closed ports, 11621 filtered ports
Reason: 53899 resets and 11621 no-responses
PORT      STATE SERVICE       REASON          VERSION
21/tcp    open  ftp           syn-ack ttl 127 Microsoft ftpd
|_ftp-anon: Anonymous FTP login allowed (FTP code 230)
| ftp-syst:
|_  SYST: Windows_NT
80/tcp    open  http          syn-ack ttl 127 Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-title: Home - Acme Widgets
111/tcp   open  rpcbind       syn-ack ttl 127 2-4 (RPC #100000)
| rpcinfo:
|   program version   port/proto  service
|   100000  2,3,4        111/tcp  rpcbind
|   100000  2,3,4        111/udp  rpcbind
|   100003  2,3         2049/udp  nfs
|   100003  2,3,4       2049/tcp  nfs
|   100005  1,2,3       2049/tcp  mountd
|   100005  1,2,3       2049/udp  mountd
|   100021  1,2,3,4     2049/tcp  nlockmgr
|   100021  1,2,3,4     2049/udp  nlockmgr
|   100024  1           2049/tcp  status
|_  100024  1           2049/udp  status
135/tcp   open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
139/tcp   open  netbios-ssn   syn-ack ttl 127 Microsoft Windows netbios-ssn
445/tcp   open  microsoft-ds? syn-ack ttl 127
2049/tcp  open  mountd        syn-ack ttl 127 1-3 (RPC #100005)
5985/tcp  open  http          syn-ack ttl 127 Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
47001/tcp open  http          syn-ack ttl 127 Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
49664/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
49665/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
49666/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
49678/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
49679/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
49680/tcp open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| p2p-conficker:
|   Checking for Conficker.C or higher...
|   Check 1 (port 45222/tcp): CLEAN (Couldn't connect)
|   Check 2 (port 57430/tcp): CLEAN (Couldn't connect)
|   Check 3 (port 33056/udp): CLEAN (Failed to receive data)
|   Check 4 (port 15893/udp): CLEAN (Timeout)
|_  0/4 checks are positive: Host is CLEAN or ports are blocked
| smb2-security-mode:
|   2.02:
|_    Message signing enabled but not required
| smb2-time:
|   date: 2020-07-10 13:04:43
|_  start_date: 1601-01-01 00:09:21

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Fri Jul 10 13:00:23 2020 -- 1 IP address (1 host up) scanned in 217.56 seconds
```

### 

## Vulnerabilities
###
```

